# gsp-chrome-extension-sources
## License
This GitLab project provides a collection of JSON files containing 
- proxy host:port data
- user_agents data
- Accept-Language values

Which can be used by a [GSP](https://chrome.google.com/webstore/detail/geoshiftpro/difekemhacmecfonakeeieedhjaghbbe?hl=en&authuser=1) Chrome extension for browsing the internet securely and anonymously. 

The project is public and free to use, meaning that anyone can access and edit the data to support this open project.


# Please keep the project clean

Please keep the project clean and free of any malicious content. If you find any malicious content, please report it to the project owner. Here are some guidelines to keep the project clean and its data accurate and uniform:

## [langs.json](./langs.json)

This JSON file contains a mapping of countries to language codes that could be used in the Accept-Language header of an HTTP request. Each key is a country name, and its corresponding value is a language code.

The language codes are formatted according to the BCP 47 standard, which specifies how to tag languages and regional dialects. The language code is a combination of a language subtag (e.g., "en" for English) and a region subtag (e.g., "us" for United States). The two subtags are separated by a hyphen ("-").

Some of the country names have multiple language codes associated with them, which means that there are multiple languages commonly spoken in those countries.
### Structure for langs.json

```js
{
    <country> : <language_code>-<region_code>,
    <country> : <language_code>-<region_code>,
    ...
}
```

## [proxies.json](./proxies.json)
This Json file contains a list of available proxies to the open public. Please note that the proxies are not guaranteed to be anonymous or secure as well as working. A CI/CD pipeline is used to check the proxies for availability and to remove any dead proxies. 

### Structure for proxies.json

```js
{
    <country> : {
        "proxies" : [
            {   
                "name" : <city/location>,
                "host" : <ipv4_address>,
                "port": <valid_port>,
                "ping": xxxx ms,
                "ping_class": [1, 2, 3]
            },
            {
                "name" : <city/location>,
                "host" : <ipv4_address>,
                "port": <valid_port>,
                "ping": xxxx ms,
                "ping_class": [1, 2, 3]
            },
            ...
        ],
        "flag" : <country_flag emoji>
    },
    <country> : {
        "proxies" : [
            {   
                "name" : <city/location>,
                "host" : <ipv4_address>,
                "port": <valid_port>,
                "ping": xxxx ms,
                "ping_class": [1, 2, 3]
            },
            {
                "name" : <city/location>,
                "host" : <ipv4_address>,
                "port": <valid_port>,
                "ping": xxxx ms,
                "ping_class": [1, 2, 3]
            },
            ...
        ],
        "flag" : <country_flag emoji>
    },
    ...
}
```

## [user_agents.json](./user_agents.json)

This JSON file contains a list of user agents that can be used in the User-Agent header of an HTTP request. The user agents are grouped by their operating system.

### Structure for user_agents.json

```js
{
    <os/group> : [
        <user_agent>:<user_agent_string>,
        <user_agent>:<user_agent_string>,
        ...
    ],
    <os/group> : [
        <user_agent>:<user_agent_string>,
        <user_agent>:<user_agent_string>,
        ...
    ],
    ...
}
```






