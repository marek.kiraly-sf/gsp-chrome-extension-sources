"""
    Tool that filters the proxies from the Proxyscrape API and keeps only the fastest ones. Additionally, it adds the
    geolocation information to each proxy using the ipinfo API.

    Author: Marek Kiraly
"""
from datetime import datetime
import subprocess
import platform
import json
import os
import ipinfo
from collections import defaultdict

import numpy as np
import requests
from time import monotonic

PROXY_LOG = "proxy_filter.log"
OUT_FILE = "proxies.json"
ACCESS_TOKEN = os.getenv("IP_INFO_TOKEN")
IP_ADDRESS_LIMIT_PER_RUN: int = 100

def log_event(event: str) -> None:
    """
        Logs an event to the beginning of the log file.

        :param event: The event to log
    """
    if not os.path.exists('./artifacts'):
        os.mkdir('./artifacts')
        
    if not os.path.exists(f'./artifacts/{PROXY_LOG}'):
        with open(f'./artifacts/{PROXY_LOG}', 'w'):
            pass

    with open(f'./artifacts/{PROXY_LOG}', 'r+') as f:
        file_data = f.read() 
        f.seek(0, 0) 
        f.write(f"[{datetime.now()}] - {event}\n" + file_data) 


def ping(ip: str) -> tuple[float, str, str, str]:
    """
        Pings an IP address and returns the time it took to respond.

        :param ip: The IP address to ping

        :return: a tuple of the ping time, country_short, country name, and region-city name
    """
    try:
        if platform.system() == 'Windows':
            output = subprocess.check_output(['ping', '-n', '1', '-w', '3000', ip])
        else:
            output = subprocess.check_output(['ping', '-c', '1', '-W', '3', ip])

        output = output.decode('utf-8') 
        ping = float(output.split('time=')[1].split('ms')[0])
    except Exception:
        ping = -1.0

    handler = ipinfo.getHandler(ACCESS_TOKEN)
    details = handler.getDetails(ip)
    if "bogon" in details.details or not details or ping < 0:
        return ping, "", "", ""
    else:
        return ping, details.country, details.country_name, f"{details.region} - {details.city}",


def new_country() -> dict:
    """
        Creates a new country object.

        :return: a new country object
    """
    return {
        "proxies": [],
        "flag": ""
    }


def is_valid_ip(ip: str) -> bool:
    """
        Checks if the IP is valid.

        :param ip: The IP to check

        :return: True if the IP is valid, False otherwise
    """
    parts = ip.split(".")
    if len(parts) != 4:
        return False

    for part in parts:
        try:
            if not 0 <= int(part) <= 255:
                return False
        except ValueError:
            return False
    return True


def is_valid_port(port: str) -> bool:
    """
        Checks if the port is valid.

        :param port: The port to check

        :return: True if the port is valid, False otherwise
    """
    try:
        return 0 <= int(port) <= 65535
    except ValueError:
        return False


if __name__ == '__main__':
    start = start_time = monotonic()
    response = requests.request("GET",
                                f"https://api.proxyscrape.com/v2/?request=displayproxies"
                                f"&timeout=10000&country=all&ssl=all&anonymity=anonymous",
                                headers={},
                                data={})

    content = response.text.split()[:IP_ADDRESS_LIMIT_PER_RUN]

    output: defaultdict = defaultdict(new_country)

    # Iterate through each country
    proxy_num: int = len(content)
    alive_proxies: int = 0
    for i, ip_port in enumerate(content):
        print(f"Pinging {ip_port} ({i + 1}/{proxy_num})... elapsed time {np.round(monotonic() - start, 2)} seconds")
        ip, port = ip_port.split(":")

        # Ping the proxy
        if is_valid_ip(ip) and is_valid_port(port):
            ping_time, country_short, country, city = ping(ip)
            if ping_time < 0:
                continue
        else:
            continue
                
        alive_proxies += 1
        # If the ping was successful, add the ping time and ping class to the proxy
        output[country]['proxies'].append({
            "name": city,
            "host": ip,
            "port": port,
            "ping": ping_time,
            "ping_class": 1 if ping_time < 100 else 2 if ping_time < 200 else 3})
        output[country]["flag"] = country_short

    # Sort the proxies by ping time and keep max 10 fastest
    for country, data in output.items():
        output[country]["proxies"] = sorted(data["proxies"], key=lambda x: x["ping"])[:10]

    with open(f'./artifacts/{OUT_FILE}', 'w') as f:
        if len(output):
            print(json.dumps(output, indent=4), file=f)
        else:
            print("{}", file=f)

    log_event(f"{alive_proxies} ({np.round((alive_proxies / proxy_num) * 100, 2)}%) "
              f"proxies pinged successfully in {np.round(monotonic() - start_time, 0)} seconds")
